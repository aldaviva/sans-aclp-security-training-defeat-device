// ==UserScript==
// @name         SANS Security Training Defeat Device
// @namespace    https://bitbucket.org/aldaviva/sans-aclp-security-training-defeat-device/
// @version      0.5.0
// @description  Automate taking the useless annual SANS ACLP security training course
// @license      Apache-2.0; https://www.apache.org/licenses/LICENSE-2.0.txt
// @author       Volkswagen IT Department
// @match        https://cc.sans.org/*
// @match        https://leam.sans.org/*
// @match        https://learn.sans.org/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';
    var $ = window.$;
    var _ = window._;
    var STORAGE_KEY = "SANSSTDD";
    var localStorage = window.localStorage;

    function controlFlow(proceed) {
        localStorage.setItem(STORAGE_KEY, proceed ? "true" : "false");
    }

    function currentlyDefeating() {
        return localStorage.getItem(STORAGE_KEY) == "true";
    }

    setTimeout(function(){
        var onSelectionPage = document.querySelectorAll("#required-accordion").length > 0;
        if (onSelectionPage) {
            var module = nextIncompleteModule();
            if (!module) {
                console.log("Acknowleding completion");
                acknowledgeCompletion();
                return;
            }
            if (currentlyDefeating()) {
                giveUserInterruptChoice().then(function() {
                    // Continue as normal
                    console.log("Continuing defeat...");
                    controlFlow(true);
                    proceedToModule(module);
                }).fail(function() {
                    console.log("Cancelling defeat by user request");
                    controlFlow(false);
                });
            } else {
                giveUserStartDefeatChoice().then(function() {
                    console.log("User has chosen to [REDACTED]. Proceeding.");
                    controlFlow(true);
                    proceedToModule(module);
                }).fail(function() {
                    console.log("User has chosen not to [REDACTED]");
                    controlFlow(false);
                });
            }
        } else {
            setInterval(function(){
                chooseLanguage();
                skipVideo();
                answerQuizQuestion();
                defeatInteractiveModule();
                closeModuleWhenDone();
            }, 1000);
        }
    }, 500);

    function giveUserStartDefeatChoice(deferred) {
        var userChoiceDeferred = $.Deferred();
        // Show modeal
        var content = '' +
            '<p style="font-size: 18px; font-weight: bold;">Welcome to the SANS Security Training Defeat Device</p>' +
            '<p>This defeat device [REDACTED] the [REDACTED] for you so all you have to do is wait. Please select from [REDACTED] to [REDACTED].</p>' +
            '<div class="actions" style="margin-top:20px;">' +
            '  <p><button class="dontRedacted" style="background-color: red; margin-bottom: 14px; color: black;">I don\'t like this.</button></p>' +
            '  <p><button class="doRedacted" style="background-color: #20C20E; color: black;">Follow the white rabbit.</button></p>' +
            '</div>';
        showModalWithContent(content);
        $('.dontRedacted').on('click', function(){
            removeModal();
            userChoiceDeferred.reject().promise();
        });
        $('.doRedacted').on('click', function(){
            removeModal();
            userChoiceDeferred.resolve().promise();
        });
        return userChoiceDeferred;
    }

    function giveUserInterruptChoice() {
        var userInterruptDeferred = $.Deferred();
        var content = '' +
            '<p style="font-size: 18px; font-weight: bold;">Defeat in Progress...</p>' +
            '<p>Defeat will continue in 2 seconds.</p>' +
            '<div class="actions" style="margin-top:20px;">' +
            '  <p><button class="interrupt" style="background-color: red; margin-bottom: 14px; color: black;">STOP IT.</button></p>' +
            '</div>';
        showModalWithContent(content);
        $('.interrupt').on('click', function() {
            removeModal();
            userInterruptDeferred.reject().promise();
        });
        setTimeout(function() {
            userInterruptDeferred.resolve().promise();
        }, 2000);
        return userInterruptDeferred;
    }

    function acknowledgeCompletion() {
        var content = '' +
            '<p style="font-size: 18px; font-weight: bold;">DEFEAT COMPLETED</p>' +
            '<div class="actions" style="margin-top:20px;">' +
            '  <p><button class="certificate" style="background-color: grey; margin-bottom: 14px;">Show Certificate</button></p>' +
            '  <p><button class="dismiss" style="background-color: grey; margin-bottom: 14px;">Dismiss</button></p>' +
            '</div>';
        showModalWithContent(content);
        $('.certificate').on('click', function() {
            removeModal();
            var link = document.querySelector('.course-certificate-url a');
            link && link.click();
        });
        $('.dismiss').on('click', function() {
            removeModal();
        });
    }

    function showModalWithContent(content) {
        var modal = '' +
            '<div class="defeatWrapper" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; z-index: 9999; background-color: rgba(0, 0, 0, 0.75)">' +
            '  <div class="content" style="text-align:center; position: relative; margin: 0 auto;' +
                 'width: 400px; height: 260px; top: 200px; background-color: black; color:#20C20E; padding: 20px;">' +
            content +
            '  </div>'+
            '</div>';
        $('body').append(modal);
    }

    function removeModal() {
        $('.defeatWrapper').remove();
    }

    function nextIncompleteModule() {
        var els = document.querySelectorAll("#required-accordion td.course-completed, #required-accordion td.module-launch-url");
        for (var i = 0; i < els.length; i++) {
            var cur = els[i];
            var next = els[i+1];
            if (cur.classList.contains('course-completed') && next.classList.contains('module-launch-url')) {
                var completedSpan = cur.children[0];
                if (cur.children[0].classList.contains('hide')) {
                    var link = next.children[0];
                    return link;
                }
            }
            continue;
        }
        return null;
    }

    function proceedToModule(module) {
        module && module.click();
    }

    function chooseLanguage(){
        var chooseLanguageButton = document.getElementById("continue");
        chooseLanguageButton && chooseLanguageButton.click();
    }

    function skipVideo(){
        window.videojs && document.getElementById("mainVideo") && window.videojs("mainVideo").trigger("ended");
        if (window.AICC_Score) {
            var clickType1 = document.querySelector('[title="Click to Continue"]');
            clickType1 && clickType1.click();
            var clickType2 = document.querySelector('[title="Next Page"]');
            clickType2 && clickType2.click();
            var clickType3 = document.querySelector('[title="Start"]');
            clickType3 && clickType3.click();
            window.mejs.players.mep_0 && window.mejs.players.mep_0.setCurrentTime(999);
        }

    }

    function answerQuizQuestion(){
        var questionText = $ && $("h2").text();

        if (window.score1) {
            var clickYes = document.querySelector('[title="Yes"]');
            clickYes && clickYes.click();
            var clickNext = document.querySelector('[title="Next"]');
            clickNext && clickNext.click();
            var clickFinish = document.querySelector('[title="Finish"]');
            clickFinish && clickFinish.click();
        }

        if(window.courseware && window.courseware.modules && questionText){
            var question = _.find(window.courseware.modules.quiz, { text: questionText });
            var answerText = _.find(question.answers, "correct").text;

            $(".answerText").each(function(answerElIndex, answerEl){
                answerEl = $(answerEl);
                if(answerEl.text() === answerText){
                    answerEl.closest("label").click();
                    $("#quizSubmitButton").click();
                    $("#nextQuestionButton").click();

                    return false;
                }
            });
        }
    }

    function closeModuleWhenDone() {
        var exitButton = document.querySelector("a.exit");
        exitButton && exitButton.click();
        var exitButtonType2 = document.querySelector('[title="Click Here to Return to the Home Screen"]');
        exitButtonType2 && exitButtonType2.click();
        var exitButtonType3 = document.querySelector('[title="Close"]');
        exitButtonType3 && exitButtonType3.click();
    }

    function defeatInteractiveModule() {
        var cp = window.cp;
        if(cp && !window.defeatingInteractiveModule){
            window.defeatingInteractiveModule = true; //don't rerun this whole function every second, just start it once when the page is loaded

            var deferred = $.Deferred().resolve().promise();

            deferred.then(thenWait())
                .then(function(){
                    console.log("Setting language and advancing to the next slide");
                    window.SelectedLanguage = 'English';
                    cp.jumpToNextSlide();
                })
                .then(thenWait())
                .then(function(){
                    console.log("Going to the first quiz question.");
                    cp.returnToQuiz(); //skip past the movie to the first quiz question
                })
                .then(thenWait())
                .then(function(){
                    var questionsAnsweredDeferred = $.Deferred().resolve().promise();
                    $.each(cp.movie.questions, function(submitShapeIndex, submitShapeId){
                        var questionShapeId = submitShapeId.replace(/q\d+$/, "");
                        questionsAnsweredDeferred = questionsAnsweredDeferred
                            .then(function(){
                                console.log("Answering question "+(submitShapeIndex+1)+" ("+questionShapeId+") correctly and going to the next slide.");
                                cp.SubmitInteractions(questionShapeId, cp.QuestionStatusEnum.CORRECT, 0); //give ourselves credit for correctly answering the question
                                cp.goToNextSlide(); //move to the next question
                            })
                            .then(thenWait());
                    });
                    return questionsAnsweredDeferred;
                })
                .then(function(){
                    // Intercept score node to backhack module success
                    window.userScore = 100;
                    window.userScorePercent = 100;

                    var skipManySlidesPromise = $.Deferred().resolve().promise();
                    console.log("All questions answered, skipping the post-quiz summaries and going to the score screen.");
                    for(var i = 0; i < 15; i++){ //it's safe to call cp.goToNextSlide() a bunch of times, it will eventually stop at the score screen and not progress any further
                        skipManySlidesPromise = skipManySlidesPromise
                            .then(function(){
                                cp.goToNextSlide(); //skip the post-quiz summaries (there may be more than one) and go to the score screen
                            })
                            .then(thenWait(200));
                    }
                    return skipManySlidesPromise;
                })
                .then(thenWait())
                .then(function(){
                    console.log("On the score screen, closing the pop-up window.");
                    cp.closeMovie(); //close the pop-up window
                });
        }
    }

    function thenWait(waitDurationMilliseconds){
        return function(){
            var deferred = $.Deferred();
            setTimeout(function(){
                deferred.resolve();
            }, waitDurationMilliseconds || 1000);
            return deferred.promise();
        };
    }
})();
