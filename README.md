![SANS ACLP](https://login.sans.org/login/module.php/sans/aclp-responsive/logo.png)

# SANS ACLP Security Training Defeat Device

* [Problem](#markdown-header-problem)
* [Solution](#markdown-header-solution)
* [Demonstration](#markdown-header-demonstration)
* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Updates](#markdown-header-updates)
    + [Compatibility](#markdown-header-compatibility)
    + [Automatic updates](#markdown-header-automatic-updates)
    + [Manual updates](#markdown-header-manual-updates)
* [Uninstallation](#markdown-header-uninstallation)
* [Reporting bugs](#markdown-header-reporting-bugs)

## Problem
We have to take a useless online security training course every year to pay lip service to some compliance program.
The content is the same every year, we learn nothing, and it's a huge waste of our time.

**To be clear:** the SANS training course is offensively, criminally stupid, and we're insulted we have to deal with the same garbage every year. We have far exceeded the requirements of understanding computer security by developing this script.

## Solution
Now your computer can finish the course for you with the click of a single button! It will skip the videos and answer the questions for you. 
It even chooses your language each time, as if that would ever change (stupid!).

The technique employed here is a user script that runs in your browser. It interacts with the SANS user interface using the DOM and JavaScript. It does not communicate with the SANS backend or any other servers.

## Demonstration
Watch the [demo video](https://www.youtube.com/watch?v=FVruvN3DXcU) to see the user script in action, completing the entire 2019 General Security Awareness Training with one click.

[![SANS Cybersecurity Training Defeat Device demo video](https://i.ytimg.com/vi/FVruvN3DXcU/hqdefault.jpg)](https://www.youtube.com/watch?v=FVruvN3DXcU)

## Installation
1. Install a user script extension in your browser. Some available implementations to pick from:
    - [**Tampermonkey**](https://tampermonkey.net/) works in Chrome, Firefox, Edge (not IE), Safari, Opera, and Vivaldi.
    - [Violentmonkey](https://violentmonkey.github.io/get-it/) works in Chrome, Firefox, Opera, and Vivaldi.
    - [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) works in Firefox.
2. Open the [user script file](https://bitbucket.org/aldaviva/sans-aclp-security-training-defeat-device/raw/master/sans_defeat.user.js) and you should see a Tampermonkey page appear.
3. Click the **Install** button.

*Instructions are written for Tampermonkey 4.6. Other extensions may need different steps.*

## Usage
1. Make sure your computer won't go to sleep for the next few minutes, which would interrupt the training. You may want to check your power saving settings, ensure you're not running on battery power, or use a program like [Caffeine](http://lightheadsw.com/caffeine/) to keep your computer awake.
2. Log in to the [SANS ACLP training page](https://leam.sans.org/learner_dashboard), or log in through your single sign-on identity provider.
3. Choose to proceed with defeat device when prompted.
    - You will need to allow pop-up windows in your browser so ACLP can open the modules.
    - After each module is completed you will have a chance to interrupt the process. If you do nothing, the process will continue automatically with the next incomplete module until all required modules are completed.
4. Once all the required modules have green check marks next to them, you're done.
5. Forget about this crap for precisely one year.

## Updates

### Compatibility
This version of the script is compatible with the **June 2019** version of SANS Cybersecurity Awareness training.

If this script stops working, make sure you're using the latest version.

### Automatic updates
Tampermonkey will automatically check for updates to this script every day by default. You can change this frequency from the Settings tab on the Tampermonkey Dashboard.

### Manual updates
1. Open the Tampermonkey Dashboard from the browser toolbar button or extension list.
2. Check the box next to SANS Security Training Defeat Device.
3. Use the action dropdown to choose Trigger Update.
4. Click Start.

## Uninstallation
1. Open the Tampermonkey Dashboard from the browser toolbar button or list of extensions.
2. Disable or delete the SANS Security Training Defeat Device item.
3. If you weren't using Tampermonkey for anything else, you can uninstall the entire extension as well, if you want.

## Reporting bugs
If the Installation and Usage steps do not help you automatically complete a training module, please [file an issue](https://bitbucket.org/aldaviva/sans-aclp-security-training-defeat-device/issues) with all of the following information.

* the link to a screenshot of the page and Developer Tools Console on an image hosting service like Imgur
* your operating system and version (*e.g.* Windows 7)
* your browser and version (*e.g.* Chrome 65)
* the name of the training module that failed to complete (*e.g.* You Are The Shield)
* any errors that appear in your browser console (right click, Inspect Element)
* what you wanted to happen (*e.g.* it should have answered the quiz question about bribes correctly and then advanced to the next question)
* what actually happened (*e.g.* it selected the radio button for the wrong answer about Microsoft Office, and got stuck after the Wrong Answer message appeared)
* the last thing you saw before it all went haywire (*e.g.* the language selection screen, with US English selected)
